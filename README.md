# Project Major Functions
1. Jwt token authentication 
2. Make active user, reset password via mail confirm(use AWS SES)
3. AWS rds connect
4. File upload with presigned url , front-end(angular) upload to s3 directly
5. Make AWS Api gateway and Lambda function to process uploaded file 
6. Backend(flask) convert to lambda
7. Frontend(angular5 dist assets) upload to s3

## Build

  - Database migration
      
          python db_create.py
  - Build Frontend(in angular5 cli)
      
          ng build prod
                    
    
  - Get "dist" folder as result. Then move it in backend folder. call index.html in "templates" folder.
      
        app = Flask(__name__, template_folder="templates", static_folder="dist", static_url_path="")            
        @app.route('/')        
        def index():    
            return render_template("index.html", s3_url=app.config['S3_URL'])
   
    Modify index.html file
    
          <script src="{{ s3_url }}assets/js/jquery.min.js"></script>
          <script src="{{ s3_url }}assets/js/jquery.dropotron.min.js"></script>
          <script src="{{ s3_url }}assets/js/jquery.scrolly.min.js"></script>
          <script src="{{ s3_url }}assets/js/skel.min.js"></script>
          <script src="{{ s3_url }}assets/js/util.js"></script> 
  
## Make Serverless backend and frontend 
### Convert backend to AWS Lambda with zappa, This is just backend hosting.
    
  - Config aws cli      
    
        $pip install awscli
        
        $ aws configure        
        
   
  - Install and run zappa
    
         $ pip install zappa
         $ zappa init
         $ zappa deploy prod
         
   Get endpoint as result.       
   reference url :    https://github.com/Miserlou/Zappa
            

### Upload frontend to S3 , This is just frontend hosting.
    
   - upload assets files in dist folder to zappa-li bucket
   
   All files in dist folder upload to s3 with flasks3, or manually.
        
   Using flask_s3 :        
          
            python assets_upload.py 
         
       
       
 