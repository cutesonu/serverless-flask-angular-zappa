from run import s3, app
import flask_s3

def upload_assets():
    app.config['FLASKS3_BUCKET_NAME'] = 'zappa-liu'
    flask_s3.create_all(app)
    print('completed\n')

if __name__ == '__main__':
    upload_assets()