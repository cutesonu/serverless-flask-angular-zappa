
class Config(object):
    DEBUG = False
    TESTING = False

    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://[USRE_NAME]:[PASSWORD]@[RDS_URL]/[DB_NAME]'
    SECRET_KEY = ''
    AWS_ACCESS_KEY_ID = ''
    AWS_SECRET_ACCESS_KEY = ''
    BUCKET = ''

    SES_REGION_NAME = 'us-east-1'  # change to match your region
    SES_EMAIL_SOURCE = ''           # source mail or domain
    S3_URL = ' '                    # S3_URL = 'https://s3.amazonaws.com/mybucket/'
    SITE_DOMAIN = 'http://localhost:5000/'


class DefaultConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True