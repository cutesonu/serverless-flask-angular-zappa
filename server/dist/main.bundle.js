webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home_component__ = __webpack_require__("./src/app/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__not_found_notfound_component__ = __webpack_require__("./src/app/not-found/notfound.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__source_form_upload_component__ = __webpack_require__("./src/app/source/form-upload.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__user_signup_user_signup_component__ = __webpack_require__("./src/app/user-signup/user-signup.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__user_signin_user_signin_component__ = __webpack_require__("./src/app/user-signin/user-signin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_guards_auth_guard_service__ = __webpack_require__("./src/app/shared/guards/auth-guard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__user_signin_change_password_component__ = __webpack_require__("./src/app/user-signin/change-password.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__user_signin_send_passwordlink_component__ = __webpack_require__("./src/app/user-signin/send-passwordlink.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__home_home_component__["a" /* HomeComponent */] },
    { path: 'sources/s3upload', component: __WEBPACK_IMPORTED_MODULE_4__source_form_upload_component__["a" /* FormUploadComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_7__shared_guards_auth_guard_service__["a" /* AuthGuard */]] },
    { path: 'signup', component: __WEBPACK_IMPORTED_MODULE_5__user_signup_user_signup_component__["a" /* UserSignUpComponent */] },
    { path: 'signin', component: __WEBPACK_IMPORTED_MODULE_6__user_signin_user_signin_component__["a" /* UserSignInComponent */] },
    { path: 'forgot', component: __WEBPACK_IMPORTED_MODULE_9__user_signin_send_passwordlink_component__["a" /* ForgotPasswordComponent */] },
    { path: 'resetpwd', component: __WEBPACK_IMPORTED_MODULE_8__user_signin_change_password_component__["a" /* ChangePasswordComponent */] },
    { path: '**', component: __WEBPACK_IMPORTED_MODULE_3__not_found_notfound_component__["a" /* NotFoundComponent */] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forRoot(routes, { useHash: true })],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ".form-control {\r\n  width: 90%;\r\n  border: 1px solid #e5e6e7 !important;\r\n}\r\n.loading-button{\r\n  font-size: 14px;\r\n}\r\n#time-picker-wrapper {\r\n  z-index: 1500;\r\n}\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<header id=\"header\" class=\"fix reveal\">\r\n\r\n  <a id=\"logo\" href=\"#\" style=\"left: 0px; height: 70px \">\r\n    <img  src=\"../assets/bird.png\" style=\"display: inline; height: 50px;\">\r\n  </a>\r\n\r\n  <nav id=\"nav\">\r\n    <ul>\r\n       <li class=\"current\" *ngIf=\"loggedInData.username =='' || loggedInData.username == undefined\">\r\n         <a [routerLink]=\"['']\">Welcome</a>\r\n       </li>\r\n      <li class=\"current\" *ngIf=\"loggedInData.username!=''\">\r\n         <span style=\"color: #ff8c00\">{{loggedInData.username}}</span>\r\n       </li>\r\n      <li class=\"submenu\">\r\n        <a href=\"#\">Menu</a>\r\n        <ul>\r\n          <li><a routerLink=\"/signup\">Sign UP</a></li>\r\n          <li><a routerLink=\"/signin\">Sign In</a></li>\r\n          <li><a routerLink=\"/sources/s3upload\">S3 Upload</a></li>\r\n\r\n        </ul>\r\n      </li>\r\n      <li *ngIf=\"!isLoggedIn\"><a routerLink=\"/signup\" class=\"button special\">Sign Up</a></li>\r\n      <li *ngIf=\"isLoggedIn\"><a (click)=\"logout()\" class=\"button special\">Sign Out</a></li>\r\n    </ul>\r\n  </nav>\r\n\r\n</header>\r\n\r\n<router-outlet>\r\n\r\n</router-outlet>\r\n\r\n<footer id=\"footer\">\r\n\r\n  <br>\r\n  <ul class=\"copyright\">\r\n    <li>\r\n      <a href=\"#\">\r\n        <img src=\"\" style=\"display: inline; height: 50px;\">\r\n      </a>\r\n    </li>\r\n  </ul>\r\n  <ul class=\"copyright\">\r\n    <li>&copy;2018  </li>\r\n  </ul>\r\n  <ul class=\"copyright\">\r\n    <li><a href=\"#\">Contact</a></li><li><a href=\"#\">Privacy</a></li>\r\n  </ul>\r\n\r\n</footer>\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_services_user_service__ = __webpack_require__("./src/app/shared/services/user.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*tslint:disable*/



var AppComponent = /** @class */ (function () {
    function AppComponent(router, route, userService) {
        this.router = router;
        this.route = route;
        this.userService = userService;
    }
    AppComponent.prototype.ngOnInit = function () { };
    Object.defineProperty(AppComponent.prototype, "isLoggedIn", {
        /**
        * Is the user logged in?
        */
        get: function () {
            return this.userService.isLoggedIn();
        },
        enumerable: true,
        configurable: true
    });
    /**
    * Log the user out
    */
    AppComponent.prototype.logout = function () {
        var _this = this;
        this.userService.logout().subscribe(function (data) {
            _this.router.navigate(['/signin']);
        }, function (err) {
            console.log(err);
            _this.router.navigate(['/signin']);
        });
    };
    Object.defineProperty(AppComponent.prototype, "loggedInData", {
        get: function () {
            return this.userService.getLoggedInData();
        },
        enumerable: true,
        configurable: true
    });
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__shared_services_user_service__["a" /* UserService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.constant.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettings; });
var AppSettings = /** @class */ (function () {
    function AppSettings() {
    }
    // public static API_ENDPOINT = '';
    AppSettings.API_ENDPOINT = 'http://localhost:5000';
    return AppSettings;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_routing_module__ = __webpack_require__("./src/app/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home_module__ = __webpack_require__("./src/app/home/home.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__not_found_notfound_component__ = __webpack_require__("./src/app/not-found/notfound.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_file_upload__ = __webpack_require__("./node_modules/ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__shared_services_source_service__ = __webpack_require__("./src/app/shared/services/source.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ngx_toastr__ = __webpack_require__("./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__source_form_upload_component__ = __webpack_require__("./src/app/source/form-upload.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__user_signup_user_signup_component__ = __webpack_require__("./src/app/user-signup/user-signup.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__shared_services_user_service__ = __webpack_require__("./src/app/shared/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__user_signin_user_signin_component__ = __webpack_require__("./src/app/user-signin/user-signin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__shared_guards_auth_guard_service__ = __webpack_require__("./src/app/shared/guards/auth-guard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__user_signin_change_password_component__ = __webpack_require__("./src/app/user-signin/change-password.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__user_signin_send_passwordlink_component__ = __webpack_require__("./src/app/user-signin/send-passwordlink.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */], __WEBPACK_IMPORTED_MODULE_5__not_found_notfound_component__["a" /* NotFoundComponent */], __WEBPACK_IMPORTED_MODULE_11__source_form_upload_component__["a" /* FormUploadComponent */], __WEBPACK_IMPORTED_MODULE_12__user_signup_user_signup_component__["a" /* UserSignUpComponent */], __WEBPACK_IMPORTED_MODULE_15__user_signin_user_signin_component__["a" /* UserSignInComponent */], __WEBPACK_IMPORTED_MODULE_18__user_signin_send_passwordlink_component__["a" /* ForgotPasswordComponent */], __WEBPACK_IMPORTED_MODULE_17__user_signin_change_password_component__["a" /* ChangePasswordComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_14__angular_forms__["a" /* FormsModule */], __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */], __WEBPACK_IMPORTED_MODULE_14__angular_forms__["b" /* ReactiveFormsModule */], __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */], __WEBPACK_IMPORTED_MODULE_3__app_routing_module__["a" /* AppRoutingModule */], __WEBPACK_IMPORTED_MODULE_4__home_home_module__["a" /* HomeModule */], __WEBPACK_IMPORTED_MODULE_6_ng2_file_upload__["FileUploadModule"], __WEBPACK_IMPORTED_MODULE_9__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_10_ngx_toastr__["a" /* ToastrModule */].forRoot()
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_16__shared_guards_auth_guard_service__["a" /* AuthGuard */], __WEBPACK_IMPORTED_MODULE_13__shared_services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_8__shared_services_source_service__["a" /* SourceService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Banner -->\r\n<div id=\"banner\">\r\n  <div  class=\"container\" >\r\n    <div >\r\n      <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n          <!--Logo-->\r\n          <div class=\"text-center\" style=\"margin: 250px 20px\">\r\n            <h1 style=\"font-size: 58px;color: black\">\r\n              <strong> Flask Angular Lambda APP</strong>\r\n            </h1>\r\n          </div>\r\n          <section class=\"try\" >\r\n            <div class=\"container\">\r\n              <div class=\"text-center\">\r\n                <a routerLink=\"/sources/s3upload\" routerLinkActive=\"active\">TRY IT FREE</a>\r\n              </div>\r\n            </div>\r\n          </section>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"row\" style=\"height: 10px\"></div>\r\n"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_services_source_service__ = __webpack_require__("./src/app/shared/services/source.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_toastr__ = __webpack_require__("./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/* tslint:disable*/




var HomeComponent = /** @class */ (function () {
    function HomeComponent(router, route, sourceService, toastService) {
        this.router = router;
        this.route = route;
        this.sourceService = sourceService;
        this.toastService = toastService;
        this.cloudBackgroundShow = false;
    }
    HomeComponent.prototype.ngOnInit = function () { };
    HomeComponent.prototype.testing = function () {
        var _this = this;
        this.sourceService.GettingUser().subscribe(function (data) {
            _this.toastService.success(data);
        }, function (err) {
            _this.toastService.error(err);
        });
    };
    HomeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: __webpack_require__("./src/app/home/home.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__shared_services_source_service__["a" /* SourceService */],
            __WEBPACK_IMPORTED_MODULE_3_ngx_toastr__["b" /* ToastrService */]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/home/home.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__home_routing__ = __webpack_require__("./src/app/home/home.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_component__ = __webpack_require__("./src/app/home/home.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



/*tslint:disable*/
var HomeModule = /** @class */ (function () {
    function HomeModule() {
    }
    HomeModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__home_routing__["a" /* homeRouting */],
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home_component__["a" /* HomeComponent */]
            ],
            providers: [],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], HomeModule);
    return HomeModule;
}());



/***/ }),

/***/ "./src/app/home/home.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return homeRouting; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");

var homeRoutes = [];
/*tslint:disable*/
var homeRouting = __WEBPACK_IMPORTED_MODULE_0__angular_router__["c" /* RouterModule */].forRoot(homeRoutes, { useHash: true });


/***/ }),

/***/ "./src/app/not-found/notfound.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotFoundComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var NotFoundComponent = /** @class */ (function () {
    function NotFoundComponent() {
    }
    NotFoundComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n    i am the notfound component page\n  "
        })
    ], NotFoundComponent);
    return NotFoundComponent;
}());



/***/ }),

/***/ "./src/app/shared/guards/auth-guard.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_user_service__ = __webpack_require__("./src/app/shared/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_toastr__ = __webpack_require__("./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*tslint:disable*/
var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, userService, toastrService) {
        this.router = router;
        this.userService = userService;
        this.toastrService = toastrService;
    }
    AuthGuard.prototype.canActivate = function () {
        if (this.userService.isLoggedIn() == true) {
            // console.log('<I am a active user.>')
            return true;
        }
        else {
            localStorage.clear();
            this.toastrService.error('Authentication Error', 'Error');
            this.router.navigate(['/signin']);
            return false;
        }
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_3_ngx_toastr__["b" /* ToastrService */]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/shared/models/user.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
/*tslint:disable*/
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "./src/app/shared/services/source.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SourceService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_constant__ = __webpack_require__("./src/app/app.constant.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_do__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_observable_throw__ = __webpack_require__("./node_modules/rxjs/_esm5/add/observable/throw.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__user_service__ = __webpack_require__("./src/app/shared/services/user.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/*tslint:disable*/
var SourceService = /** @class */ (function () {
    function SourceService(router, userService, http) {
        this.router = router;
        this.userService = userService;
        this.http = http;
        this.apiUrl = __WEBPACK_IMPORTED_MODULE_2__app_constant__["a" /* AppSettings */].API_ENDPOINT;
    }
    SourceService.prototype.GettingPresignedURL = function (send_data) {
        var access_token = localStorage.getItem('accessToken');
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Accept': 'application/json' });
        headers.append("Authorization", "Bearer " + access_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.post(this.apiUrl + "/api/getpresignedurl/", send_data, options)
            .map(function (res) { return res.json(); })
            .do(function (res) {
            // console.log('here')
            // console.log(res)
        })
            .catch(this.handleError);
    };
    SourceService.prototype.Upload_By_PresignedURL = function (purl, file) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': file.type });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.put(purl, file, options)
            .map(function (res) {
            console.log(res);
            return res;
        })
            .catch(this.handleError);
    };
    SourceService.prototype.Process_Lambda_By_API = function (filename) {
        var lambda_api_gateway = '';
        var body = { filename: filename };
        return this.http.post(lambda_api_gateway, body)
            .map(function (res) {
            console.log(res);
            return res;
        })
            .catch(this.handleError);
    };
    SourceService.prototype.GettingUser = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Accept': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this.apiUrl + "/api/users/1", options)
            .map(function (res) { return res.json(); })
            .do(function (res) {
            // console.log('here')
            // console.log(res)
        })
            .catch(this.handleError);
    };
    /**
     * Handle any errors from the API
     */
    SourceService.prototype.handleError = function (err) {
        var errMessage;
        if (err instanceof __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* Response */]) {
            var body = err.json() || '';
            var error = body.error || JSON.stringify(body);
            errMessage = (err.statusText || '') + " " + error;
        }
        else {
            errMessage = err.message ? err.message : err.toString();
        }
        if (errMessage.indexOf('Token has expired') > 0) {
            localStorage.clear();
            this.userService.loggedInData = {};
            this.userService.loggedIn = false;
        }
        return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["a" /* Observable */].throw(errMessage);
    };
    SourceService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_9__user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], SourceService);
    return SourceService;
}());



/***/ }),

/***/ "./src/app/shared/services/user.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_constant__ = __webpack_require__("./src/app/app.constant.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*tslint:disable*/




var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
        this.apiUrl = __WEBPACK_IMPORTED_MODULE_3__app_constant__["a" /* AppSettings */].API_ENDPOINT;
        this.loggedIn = false;
        this.loggedInData = {};
        this.loggedIn = !!localStorage.getItem('accessToken');
        if (this.loggedIn)
            try {
                this.loggedInData.username = localStorage.getItem('username');
            }
            catch (e) {
                alert(e); // error in the above string (in this case, yes)!
            }
    }
    /**
     * Check if the user is logged in
     */
    UserService.prototype.isLoggedIn = function () {
        return this.loggedIn;
    };
    /**
     * Check if the user is logged in
     */
    UserService.prototype.getLoggedInData = function () {
        return this.loggedInData;
    };
    UserService.prototype.sendMailFor = function (username) {
        var _this = this;
        var body = { username: username };
        return this.http.post(this.apiUrl + "/api/SendMailForPassword/", body)
            .map(function (res) { return res.json(); })
            .do(function (res) {
            _this.loggedIn = false;
            _this.loggedInData = [];
        })
            .catch(this.handleError);
    };
    UserService.prototype.changePasswordFor = function (username, userpass) {
        var _this = this;
        var body = {
            username: username,
            password: userpass
        };
        return this.http.post(this.apiUrl + "/api/ChangePassword/", body)
            .map(function (res) { return res.json(); })
            .do(function (res) {
            _this.loggedIn = false;
            _this.loggedInData = [];
        })
            .catch(this.handleError);
    };
    /**
     * Log the user in
     */
    UserService.prototype.login = function (username, password) {
        var _this = this;
        return this.http.post(this.apiUrl + "/api/login/", { username: username, password: password })
            .map(function (res) { return res.json(); })
            .do(function (res) {
            if (res['accessToken']) {
                _this.loggedIn = true;
                _this.loggedInData.username = username;
                localStorage.setItem('accessToken', res['accessToken']);
                localStorage.setItem('refreshToken', res['refreshToken']);
                localStorage.setItem('username', username);
            }
            else {
                _this.loggedIn = false;
                _this.loggedInData = [];
                localStorage.removeItem('accessToken');
                localStorage.removeItem('refreshToken');
                localStorage.removeItem('username');
            }
        })
            .catch(this.handleError);
    };
    /**
     * Log the user in
     */
    UserService.prototype.register = function (username, password, last_name, first_name) {
        var _this = this;
        var userData = {
            username: username,
            password: password,
            firstname: first_name,
            lastname: last_name
        };
        return this.http.post(this.apiUrl + "/api/registration/", userData)
            .map(function (res) { return res.json(); })
            .do(function (res) {
            if (res['accessToken']) {
                _this.loggedIn = true;
                _this.loggedInData.username = username;
                localStorage.setItem('accessToken', res['accessToken']);
                localStorage.setItem('refreshToken', res['refreshToken']);
                localStorage.setItem('username', username);
            }
            else {
                _this.loggedIn = false;
                _this.loggedInData = [];
                localStorage.removeItem('accessToken');
                localStorage.removeItem('refreshToken');
                localStorage.removeItem('username');
            }
        })
            .catch(this.handleError);
    };
    /**
     * Log the user out
     */
    UserService.prototype.logout = function () {
        var _this = this;
        var access_token = localStorage.getItem('accessToken');
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Accept': 'application/json' });
        headers.append("Authorization", "Bearer " + access_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.post(this.apiUrl + "/api/logout/access/", {}, options)
            .map(function (res) { return res.json(); })
            .do(function (res) {
            localStorage.clear();
            _this.loggedInData = {};
            _this.loggedIn = false;
        })
            .catch(this.handleError);
    };
    /**
     * Handle any errors from the API
     */
    UserService.prototype.handleError = function (err) {
        var errMessage;
        if (err instanceof __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* Response */]) {
            var body = err.json() || '';
            var error = body.error || JSON.stringify(body);
            errMessage = (err.statusText || '') + " " + error;
        }
        else {
            errMessage = err.message ? err.message : err.toString();
        }
        if (errMessage.indexOf('Token') > 0) {
            localStorage.removeItem('accessToken');
            localStorage.removeItem('refreshToken');
            this.loggedInData = {};
            this.loggedIn = false;
        }
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(errMessage);
    };
    UserService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/source/form-upload.component.html":
/***/ (function(module, exports) {

module.exports = "<article id=\"main\">\r\n  <section class=\"wrapper style4 container\" style=\"min-height:500px;color:#000303;background: none\">\r\n    <div class=\"rounded-corner\">\r\n      <label class=\"btn btn-default\">\r\n      <input type=\"file\" (change)=\"selectFile($event)\">\r\n    </label>\r\n\r\n    <button class=\"btn btn-success\" [disabled]=\"!selectedFiles\" (click)=\"upload()\">\r\n\r\n\r\n                <i [ngClass]=\" uploading ? 'fa fa-gear fa-spin':'fa '\"></i>\r\n              &nbsp;Upload\r\n    </button>\r\n    <button [disabled]=\"selectedFiles == undefined\" type=\"button\" class=\"btn btn-success \" (click)=\"processDocument()\">\r\n\r\n                <i [ngClass]=\"proccessing ? 'fa fa-gear fa-spin':'fa '\"></i>\r\n              &nbsp;Process Document\r\n    </button>\r\n    </div>\r\n\r\n  </section>\r\n</article>\r\n"

/***/ }),

/***/ "./src/app/source/form-upload.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormUploadComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_services_source_service__ = __webpack_require__("./src/app/shared/services/source.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_toastr__ = __webpack_require__("./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FormUploadComponent = /** @class */ (function () {
    function FormUploadComponent(router, sourceService, toastService) {
        this.router = router;
        this.sourceService = sourceService;
        this.toastService = toastService;
        this.bucket = 'zappa-liu';
        this.folder = 'tmp/';
        this.uploading = false;
        this.proccessing = false;
    }
    FormUploadComponent.prototype.ngOnInit = function () {
    };
    FormUploadComponent.prototype.selectFile = function (event) {
        this.selectedFiles = event.target.files;
    };
    FormUploadComponent.prototype.upload = function () {
        var _this = this;
        this.uploading = true;
        var file = this.selectedFiles.item(0);
        var send_data = { filename: file.name, file_type: file.type };
        this.sourceService.GettingPresignedURL(send_data).subscribe(function (data) {
            _this.purl = data['pre_url'];
            _this.toastService.success(_this.purl, 'Presigned URL:', { disableTimeOut: true, tapToDismiss: false, closeButton: true });
            _this.sourceService.Upload_By_PresignedURL(_this.purl, file).subscribe(function (data2) {
                if (data2.status == 200)
                    _this.toastService.success('Successfully Uploaded via presigned url.', 'Success');
            }, function (error2) {
                _this.toastService.error(error2);
            });
        }, function (err) {
            console.log(err);
            if (err.indexOf('Token') > 0) {
                _this.router.navigate(['/signin']);
            }
        });
    };
    FormUploadComponent.prototype.processDocument = function () {
        var _this = this;
        var file = this.selectedFiles.item(0);
        this.sourceService.Process_Lambda_By_API(file.name).subscribe(function (data) {
            if (data.status == 200)
                _this.toastService.success('Successfully Lambda function worked.', 'Success');
        });
    };
    FormUploadComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-form-upload',
            template: __webpack_require__("./src/app/source/form-upload.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_1__shared_services_source_service__["a" /* SourceService */],
            __WEBPACK_IMPORTED_MODULE_2_ngx_toastr__["b" /* ToastrService */]])
    ], FormUploadComponent);
    return FormUploadComponent;
}());



/***/ }),

/***/ "./src/app/user-signin/change-password.component.html":
/***/ (function(module, exports) {

module.exports = "<article id=\"main\">\r\n  <section class=\"wrapper style4 container\" style=\"color:#000303;background: none\">\r\n\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\">\r\n        <!-- Logo -->\r\n        <div class=\"logo text-center\"></div>\r\n        <div class=\"text-center forms\">\r\n          <form class=\"form form-horizontal\" #userForm=\"ngForm\"\r\n                (ngSubmit)=\"sendPassword()\">\r\n\r\n            <h2>Change password for {{user_domain}}</h2>\r\n            <hr>\r\n            <div>\r\n              <label style=\"margin-left: 40px; margin-right: 50px;color: #4543a7;font-size: 17px;\">\r\n                Password must contain one lowercase letter, one number, and be at least 6 characters long.</label>\r\n            </div>\r\n            <div\r\n              [class.has-error]=\"password1.invalid && password1.touched\">\r\n              <label>Password</label> <input type=\"password\" class=\"form-control\"\r\n                                             name=\"password1\" required minlength=\"6\" [(ngModel)]=\"firs_password\"\r\n                                             #password1=\"ngModel\"> <label\r\n              class=\"control-label\"\r\n              *ngIf=\"password1.errors && password1?.errors.minlength && password1.touched\">\r\n              <i class=\"fa fa-times-circle-o\"></i> Password must be at least 6\r\n              characters long.\r\n            </label> <label  class=\"control-label\"\r\n                             *ngIf=\"password1.errors && password1?.errors.required && password1.touched\">\r\n              <i class=\"fa fa-times-circle-o\"></i> Password is required\r\n            </label>\r\n            </div>\r\n            <div\r\n              [class.has-error]=\"password2.invalid && password2.touched\">\r\n              <label>Confirm Password</label> <input type=\"password\" class=\"form-control\"\r\n                                                     name=\"password2\" required minlength=\"6\" [(ngModel)]=\"second_password\"\r\n                                                     #password2=\"ngModel\"> <label\r\n              class=\"control-label\"\r\n              *ngIf=\"password2.errors && password2?.errors.minlength && password2.touched\">\r\n              <i class=\"fa fa-times-circle-o\"></i> Password must be at least 6\r\n              characters long.\r\n            </label> <label  class=\"control-label\"\r\n                             *ngIf=\"password2.errors && password2?.errors.required && password2.touched\">\r\n              <i class=\"fa fa-times-circle-o\"></i> Password is required\r\n            </label>\r\n            </div>\r\n            <button [disabled]=\" userForm.invalid || (firs_password != second_password)\"\r\n                    class=\"btn btn-primary btn-submit\" type=\"submit\" >\r\n                  <span><i [ngClass]=\"isLoading  ? 'fa fa-gear fa-spin':'fa '\"></i>\r\n                    Change password</span>\r\n            </button>\r\n\r\n\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</article>\r\n"

/***/ }),

/***/ "./src/app/user-signin/change-password.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangePasswordComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_services_user_service__ = __webpack_require__("./src/app/shared/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_toastr__ = __webpack_require__("./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ChangePasswordComponent = /** @class */ (function () {
    function ChangePasswordComponent(userService, router, route, toastrService) {
        this.userService = userService;
        this.router = router;
        this.route = route;
        this.toastrService = toastrService;
        this.isLoading = false;
    }
    ChangePasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('<<<< change-password comp started >>>');
        this.route.queryParams.subscribe(function (params) {
            _this.username = params['username'];
            var a = _this.username.indexOf('@');
            var b = _this.username.indexOf('.');
            _this.user_domain = _this.username.substring(0, a + 1);
            // console.log(this.username);
        });
    };
    ChangePasswordComponent.prototype.sendPassword = function () {
        var _this = this;
        if (this.firs_password !== this.second_password) {
            this.toastrService.error('Password is not equal.');
            return;
        }
        this.isLoading = true;
        this.userService.changePasswordFor(this.username, this.firs_password).subscribe(function (data) {
            _this.isLoading = false;
            _this.toastrService.success(data['message'], 'Success');
            _this.router.navigate(['/signin']);
        }, function (err) {
            _this.toastrService.error(err, 'Error');
        });
    };
    ChangePasswordComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: __webpack_require__("./src/app/user-signin/change-password.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__shared_services_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_3_ngx_toastr__["b" /* ToastrService */]])
    ], ChangePasswordComponent);
    return ChangePasswordComponent;
}());



/***/ }),

/***/ "./src/app/user-signin/send-passwordlink.component.html":
/***/ (function(module, exports) {

module.exports = "<article id=\"main\">\r\n  <section class=\"wrapper style4 container\" style=\"color:#000303;background: none\">\r\n\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\">\r\n        <!-- Logo -->\r\n        <div class=\"logo text-center\"></div>\r\n        <div class=\"text-center forms\">\r\n          <form class=\"form form-horizontal\" #userForm=\"ngForm\"\r\n                (ngSubmit)=\"send()\">\r\n\r\n            <h2>Reset your password</h2>\r\n            <hr>\r\n            <div *ngIf=\"!sendOK\" class=\"username\"\r\n                 [class.has-error]=\"username.invalid && username.touched\">\r\n              <label style=\"margin-left: 40px; margin-right: 50px;color: #4543a7;font-size: 17px;\">Enter your email address and we will send you a link to reset your password.</label>\r\n              <input type=\"email\" class=\"form-control\"\r\n                     name=\"username\" required email [(ngModel)]=\"credentials.username\"\r\n                     #username=\"ngModel\">\r\n              <label\r\n                class=\"control-label\"\r\n                *ngIf=\"username.errors && username?.errors.required && username.touched\">\r\n                <i class=\"fa fa-times-circle-o\"></i> Email is required\r\n              </label>\r\n              <label class=\"control-label\"\r\n                     *ngIf=\"username.errors && username?.errors.email && username.touched\">\r\n                <i class=\"fa fa-times-circle-o\"></i> Email is not valid\r\n              </label>\r\n            </div>\r\n\r\n            <button *ngIf=\"!sendOK\" [disabled]=\" userForm.invalid\"\r\n                    class=\"btn btn-primary btn-submit\" type=\"submit\">\r\n                  <span><i [ngClass]=\"isLoading  ? 'fa fa-gear fa-spin':'fa '\"></i>\r\n                    Send password reset email\r\n                  </span>\r\n            </button>\r\n\r\n            <div *ngIf=\"sendOK\">\r\n              <label style=\"margin-left: 40px; margin-right: 30px;color: #4543a7;font-size: 17px;\">\r\n                Check your email for a link to reset your password. If it doesn't appear within a few minutes, check your spam folder.\r\n              </label>\r\n\r\n            </div>\r\n            <div *ngIf=\"sendOK\"  >\r\n              <button routerLink=\"/signin\" type=\"submit\"\r\n                      class=\"btn btn-primary btn-submit\" >\r\n                Return to sign in\r\n              </button>\r\n            </div>\r\n\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n  </section>\r\n</article>\r\n"

/***/ }),

/***/ "./src/app/user-signin/send-passwordlink.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotPasswordComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_services_user_service__ = __webpack_require__("./src/app/shared/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_toastr__ = __webpack_require__("./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*tslint:disable*/
var ForgotPasswordComponent = /** @class */ (function () {
    function ForgotPasswordComponent(userService, router, toastrService) {
        this.userService = userService;
        this.router = router;
        this.toastrService = toastrService;
        this.credentials = { username: '', password: '' };
        this.isLoading = false;
        this.sendOK = false;
    }
    ForgotPasswordComponent.prototype.ngOnInit = function () {
        // console.log("<<<< send-passwordlink-comp started >>>")
    };
    ForgotPasswordComponent.prototype.send = function () {
        var _this = this;
        var username = this.credentials.username;
        this.isLoading = true;
        this.userService.sendMailFor(this.credentials.username).subscribe(function (data) {
            _this.sendOK = true;
            _this.isLoading = false;
            _this.toastrService.success(data['message'], 'Success');
        }, function (err) {
            if (err.indexOf("exist") > -1) {
                _this.toastrService.error('Can\'t find that email, sorry.');
            }
            else
                _this.toastrService.error(err, 'Error');
        });
    };
    ForgotPasswordComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: __webpack_require__("./src/app/user-signin/send-passwordlink.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__shared_services_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3_ngx_toastr__["b" /* ToastrService */]])
    ], ForgotPasswordComponent);
    return ForgotPasswordComponent;
}());



/***/ }),

/***/ "./src/app/user-signin/user-signin.component.html":
/***/ (function(module, exports) {

module.exports = "<article id=\"main\">\r\n  <section class=\"wrapper style4 container\" style=\"color:#000303;background: none\">\r\n\r\n      <!--<div class=\"world-map\">-->\r\n      <!--<div class = \"cloud-bg-down\">-->\r\n      <div  class=\"container header-container\" >\r\n        <!--<div style=\"min-height: 650px;\">-->\r\n        <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n            <!-- Logo -->\r\n            <div class=\"logo text-center\"></div>\r\n            <div class=\"text-center forms\">\r\n              <form class=\"form form-horizontal\" #userForm=\"ngForm\"\r\n                    (ngSubmit)=\"login()\">\r\n                <label  >\r\n                  <span style=\"color: #042940\">New Account? </span>\r\n                  <a routerLink=\"/signup\" style=\"color: #0e72af\">Sign up Today</a>\r\n                </label>\r\n                <h2 style=\"color: #307b52\">Log in and Success !</h2>\r\n                <hr>\r\n                <div class=\"username\"\r\n                     [class.has-error]=\"username.invalid && username.touched\">\r\n                  <label>Email</label> <input type=\"email\" class=\"form-control\"\r\n                                              name=\"username\" required email [(ngModel)]=\"credentials.username\"\r\n                                              #username=\"ngModel\"> <label\r\n                  class=\"control-label\"\r\n                  *ngIf=\"username.errors && username?.errors.required && username.touched\">\r\n                  <i class=\"fa fa-times-circle-o\"></i> Email is required\r\n                </label> <label  class=\"control-label\"\r\n                                 *ngIf=\"username.errors && username?.errors.email && username.touched\">\r\n                  <i class=\"fa fa-times-circle-o\"></i> Email is not valid\r\n                </label>\r\n                </div>\r\n                <hr>\r\n                <div class=\"password\"\r\n                     [class.has-error]=\"password.invalid && password.touched\">\r\n                  <label ><span style=\"margin-bottom:5px; float: left; text-decoration: none\">Password</span>\r\n                        <a routerLink=\"/forgot\" class=\"label-link\" style=\"color: #0e72af;font-size: 12px\"><strong>FORGOT PASSWORD?</strong></a>\r\n                  </label>\r\n                  <input type=\"password\" class=\"form-control\"\r\n                         name=\"password\" required minlength=\"6\"\r\n                         [(ngModel)]=\"credentials.password\" #password=\"ngModel\"> <label\r\n                  class=\"control-label\"\r\n                  *ngIf=\"password.errors && password?.errors.minlength && password.touched\">\r\n                  <i class=\"fa fa-times-circle-o\"></i> Password must be at least 6\r\n                  characters long.\r\n                </label> <label  class=\"control-label\"\r\n                                 *ngIf=\"password.errors && password?.errors.required && password.touched\">\r\n                  <i class=\"fa fa-times-circle-o\"></i> Password is required\r\n                </label>\r\n                </div>\r\n                <button [disabled]=\" userForm.invalid\"\r\n                        class=\"btn btn-primary btn-submit\" type=\"submit\" >\r\n                      <span><i [ngClass]=\"isLoading  ? 'fa fa-gear fa-spin':'fa '\"></i>\r\n                      &nbsp;\r\n                      Login</span>\r\n                </button>\r\n              </form>\r\n            </div>\r\n          </div>\r\n\r\n\r\n        </div>\r\n\r\n      </div>\r\n\r\n\r\n\r\n\r\n  </section>\r\n</article>\r\n"

/***/ }),

/***/ "./src/app/user-signin/user-signin.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserSignInComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_services_user_service__ = __webpack_require__("./src/app/shared/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_toastr__ = __webpack_require__("./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*tslint:disable*/




var UserSignInComponent = /** @class */ (function () {
    function UserSignInComponent(userService, router, toastrService) {
        this.userService = userService;
        this.router = router;
        this.toastrService = toastrService;
        this.credentials = { username: '', password: '' };
        this.isLoading = false;
    }
    UserSignInComponent.prototype.ngOnInit = function () {
    };
    /**
     * Login a user
     */
    UserSignInComponent.prototype.login = function () {
        var _this = this;
        this.isLoading = true;
        this.userService.login(this.credentials.username, this.credentials.password).subscribe(function (data) {
            _this.isLoading = false;
            if (data['accessToken']) {
                _this.toastrService.success(data['message'], 'Success');
                _this.router.navigate(['/sources/s3upload']);
            }
            else {
                _this.toastrService.error(data['message'], 'Failed');
            }
        }, function (err) {
            _this.isLoading = false;
            // if (err.indexOf("Unauthorized") != -1)
            _this.toastrService.error(err, 'Error');
        });
    };
    UserSignInComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: __webpack_require__("./src/app/user-signin/user-signin.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__shared_services_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3_ngx_toastr__["b" /* ToastrService */]])
    ], UserSignInComponent);
    return UserSignInComponent;
}());



/***/ }),

/***/ "./src/app/user-signup/user-signup.component.html":
/***/ (function(module, exports) {

module.exports = "<article id=\"main\">\r\n  <section class=\"wrapper style4 container\" style=\"color:#000303;background: none\">\r\n\r\n      <div  class=\"container header-container\" >\r\n\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12\">\r\n            <!-- Logo -->\r\n            <div class=\"text-center forms\">\r\n              <form [hidden]=\"active_status\" class=\"form form-horizontal\"\r\n                    #userForm=\"ngForm\" (ngSubmit)=\"register()\">\r\n                <label>\r\n                    <span style=\"color: #042940\">\r\n                    Already have an account? </span>\r\n\r\n                  <a routerLink=\"/signin\" style=\"color: #0e72af\"\r\n                     routerLinkActive=\"active\">Log In</a>\r\n\r\n                </label>\r\n                <h2 style=\"color: #307b52\">\r\n                  JOIN<b> TODAY!</b>\r\n                </h2>\r\n                <hr>\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-6\">\r\n                    <div class=\"first_name\"\r\n                         [class.has-error]=\"first_name.invalid && first_name.touched\">\r\n                      <label>First Name</label>\r\n                      <input class=\"form-control col-sm-5\"\r\n                             name=\"first_name\" required [(ngModel)]=\"user.first_name\"\r\n                             #first_name=\"ngModel\">\r\n                      <label class=\"control-label\"\r\n                             *ngIf=\"first_name.errors && first_name?.errors.required && first_name.touched\">\r\n                        <i class=\"fa fa-times-circle-o\"></i>\r\n                        First Name is required\r\n                      </label>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"col-md-5\">\r\n                    <div class=\"last_name\"\r\n                         [class.has-error]=\"last_name.invalid && last_name.touched\">\r\n                      <label>Last Name</label>\r\n                      <input class=\"form-control col-sm-5\"\r\n                             name=\"last_name\" required [(ngModel)]=\"user.last_name\"\r\n                             #last_name=\"ngModel\">\r\n                      <label class=\"control-label\"\r\n                             *ngIf=\"last_name.errors && last_name?.errors.required && last_name.touched\">\r\n                        <i class=\"fa fa-times-circle-o\"></i> Last Name is required\r\n                      </label>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                  <hr>\r\n                  <div [class.has-error]=\"username.invalid && username.touched\">\r\n                    <label>Email</label>\r\n                    <input type=\"email\" class=\"form-control\"\r\n                           name=\"username\" required email [(ngModel)]=\"user.username\"\r\n                           #username=\"ngModel\">\r\n                    <label class=\"control-label\"\r\n                           *ngIf=\"username.errors && username?.errors.required && username.touched\">\r\n                      <i class=\"fa fa-times-circle-o\"></i> Email is required\r\n                    </label>\r\n                    <label class=\"control-label\"\r\n                           *ngIf=\"username.errors && username?.errors.email && username.touched\">\r\n                      <i class=\"fa fa-times-circle-o\"></i> Email is not valid\r\n                    </label>\r\n                  </div>\r\n                  <hr>\r\n                  <div [class.has-error]=\"password1.invalid && password1.touched\">\r\n                    <label>Password</label>\r\n                    <input type=\"password\" class=\"form-control\" (focus)=\"isMatched=true\"\r\n                           name=\"password1\" required minlength=\"6\" [(ngModel)]=\"first_password\"\r\n                           #password1=\"ngModel\"> <label class=\"control-label\"\r\n                                                        *ngIf=\"password1.errors && password1?.errors.minlength && password1.touched\">\r\n                    <i class=\"fa fa-times-circle-o\"></i> Password must be at least 6\r\n                    characters long.\r\n                  </label>\r\n                    <label class=\"control-label\"\r\n                           *ngIf=\"password1.errors && password1?.errors.required && password1.touched\">\r\n                      <i class=\"fa fa-times-circle-o\"></i> Password is required\r\n                    </label>\r\n                  </div>\r\n\r\n                  <div [class.has-error]=\"password2.invalid && password2.touched\">\r\n                    <label>Confirm Password</label>\r\n                    <input type=\"password\" class=\"form-control\" (focus)=\"isMatched=true\"\r\n                           name=\"password\" required minlength=\"6\" [(ngModel)]=\"second_password\"\r\n                           #password2=\"ngModel\">\r\n                    <label class=\"control-label\"\r\n                           *ngIf=\"password2.errors && password2?.errors.minlength && password2.touched\">\r\n                      <i class=\"fa fa-times-circle-o\"></i>\r\n                      Password must be at least 6 characters long.\r\n                    </label>\r\n                    <label class=\"control-label\"\r\n                           *ngIf=\"password2.errors && password2?.errors.required && password2.touched\">\r\n                      <i class=\"fa fa-times-circle-o\"></i>\r\n                      Password is required\r\n                    </label>\r\n                    <label class=\"control-label\"\r\n                           *ngIf=\"!isMatched\">\r\n                      <i class=\"fa fa-times-circle-o\"></i>\r\n                      DOES NOT MATCH YOUR PASSWORD\r\n                    </label>\r\n                  </div>\r\n                  <hr>\r\n\r\n                  <button [disabled]=\" userForm.invalid \"\r\n                          class=\"btn btn-primary btn-submit\" type=\"submit\">\r\n                  <span><i [ngClass]=\"isLoading  ? 'fa fa-gear fa-spin':'fa '\"></i>\r\n                  &nbsp;Register</span></button>\r\n\r\n\r\n\r\n              </form>\r\n              <form [hidden]=\"!active_status\" class=\"form form-horizontal\">\r\n                <h2 style=\"text-align: center; color: green;\">Your account must\r\n                  be approved before you can access. please check your email for an\r\n                  update shortly.</h2>\r\n              </form>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n\r\n  </section>\r\n</article>\r\n"

/***/ }),

/***/ "./src/app/user-signup/user-signup.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserSignUpComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_services_user_service__ = __webpack_require__("./src/app/shared/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_models_user__ = __webpack_require__("./src/app/shared/models/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_toastr__ = __webpack_require__("./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*tslint:disable*/
var UserSignUpComponent = /** @class */ (function () {
    function UserSignUpComponent(route, userService, router, toastrService) {
        this.route = route;
        this.userService = userService;
        this.router = router;
        this.toastrService = toastrService;
        this.user = new __WEBPACK_IMPORTED_MODULE_3__shared_models_user__["a" /* User */]();
        this.active_status = false;
        this.isMatched = true;
    }
    UserSignUpComponent.prototype.ngOnInit = function () {
    };
    UserSignUpComponent.prototype.register = function () {
        var _this = this;
        if (this.first_password != this.second_password) {
            this.isMatched = false;
            this.toastrService.error('Password is not equal.', 'Error');
            return;
        }
        this.isLoading = true;
        this.userService.register(this.user.username, this.first_password, this.user.last_name, this.user.first_name)
            .subscribe(function (data) {
            _this.active_status = true;
            _this.toastrService.success(data['message'], 'Success');
            // if(data['accessToken']){
            //   this.toastrService.success(data['message'], 'Success');
            //   this.router.navigate(['/sources/s3upload']);
            // }else {
            //   this.toastrService.error(data['message'], 'Failed');
            // }
            _this.isLoading = false;
        }, function (err) {
            _this.isLoading = false;
            _this.toastrService.error(err, 'Error');
        });
    };
    UserSignUpComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: __webpack_require__("./src/app/user-signup/user-signup.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__shared_services_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4_ngx_toastr__["b" /* ToastrService */]])
    ], UserSignUpComponent);
    return UserSignUpComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map