import boto3, os
from flask import Flask, render_template, redirect
from flask_s3 import FlaskS3
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from flask_jwt_extended import JWTManager
from flask_cors import CORS
from flask_restful import Resource, reqparse
from flask_migrate import Migrate
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required,
                                get_jwt_identity, get_raw_jwt)
from passlib.hash import pbkdf2_sha256 as sha256

app = Flask(__name__, template_folder="templates", static_folder="dist", static_url_path="")
app.config.from_object('config.DefaultConfig')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']

db = SQLAlchemy(app)
migrate = Migrate(app, db)


jwt = JWTManager(app)
CORS(app)
api = Api(app)
s3 = FlaskS3(app)


@app.route('/')
def index():
    """
        initial rendering of the web interface
    """
    # return app.send_static_file("index.html")
    return render_template("index.html", s3_url=app.config['S3_URL'])


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return RevokedTokenModel.is_jti_blacklisted(jti)


def send_email(app, recipients, sender=None, subject='', text='', html=''):
    ses = boto3.client(
        'ses',
        region_name=app.config['SES_REGION_NAME'],
        aws_access_key_id=app.config['AWS_ACCESS_KEY_ID'],
        aws_secret_access_key=app.config['AWS_SECRET_ACCESS_KEY']
    )
    if not sender:

        sender = app.config['SES_EMAIL_SOURCE']
    try:
        res = ses.send_email(
            Source=sender,
            Destination={'ToAddresses': recipients},
            Message={
                'Subject': {'Data': subject},
                'Body': {
                    'Text': {'Data': text},
                    'Html': {'Data': html}
                }
            }
        )
    except :
        return False

    return True


class UserModel(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(120), unique=True, nullable=False)
    lastname = db.Column(db.String(120), unique=True, nullable=False)
    username = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)
    active = db.Column(db.Integer, default=0)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_username(cls, username):
        results = []
        results = cls.query.filter_by(username=username)
        return cls.query.filter_by(username=username).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'username': x.username,
                'password': x.password
            }

        return {'users': list(map(lambda x: to_json(x), UserModel.query.all()))}

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)


class RevokedTokenModel(db.Model):
    __tablename__ = 'revoked_tokens'
    id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(120))

    def add(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def is_jti_blacklisted(cls, jti):
        query = cls.query.filter_by(jti=jti).first()
        return bool(query)


parser = reqparse.RequestParser()
parser.add_argument('username', help='This field cannot be blank', required=True)
parser.add_argument('password', help='This field cannot be blank', required=True)
parser.add_argument('firstname', help='This field cannot be blank', required=False)
parser.add_argument('lastname', help='This field cannot be blank', required=False)


class UserRegistration(Resource):
    def post(self):
        data = parser.parse_args()

        if UserModel.find_by_username(data['username']):
            return {'message': 'User {} already exists'.format(data['username'])}

        new_user = UserModel(
            firstname=data['firstname'],
            lastname=data['lastname'],
            username=data['username'],
            password=UserModel.generate_hash(data['password'])
        )

        try:
            new_user.save_to_db()
            recipients = [data['username']]
            subject = 'Thanks for registering'

            # You can render the message using Jinja2
            hash_username = sha256.hash(data['username'])
            html = render_template('email.html', hash_username=hash_username, username=data['username'], site_domain=app.config['SITE_DOMAIN'])

            flag = send_email(app=app, recipients=recipients, subject=subject, html=html)
            if flag:
                # access_token = create_access_token(identity=data['username'])
                # refresh_token = create_refresh_token(identity=data['username'])
                return {
                    'message': 'User {} was saved.\n Please make active account via mail verifying.'.format(data['username']),
                }
            else:
                return {
                    'message': 'User {} wasn\'t saved.\n Username should be correct email.'.format(data['username']),
                }
        except:
            return {'message': 'Something went wrong'}, 500


class UserLogin(Resource):
    def post(self):
        data = parser.parse_args()
        current_user = UserModel.find_by_username(data['username'])

        if not current_user:
            return {'message': 'User {} doesn\'t exist'.format(data['username'])}

        if UserModel.verify_hash(data['password'], current_user.password):
            if current_user.active == 1:
                access_token = create_access_token(identity=data['username'])
                refresh_token = create_refresh_token(identity=data['username'])
                return {
                    'message': 'Logged in as {}'.format(current_user.username),
                    'accessToken': access_token,
                    'refreshToken': refresh_token
                }
            else:
                return {'message': 'User {} isn\'t active user.'.format(data['username'])}
        else:
            return {'message': 'Wrong credentials'}


class UserActive(Resource):
    def get(self):
        parse = reqparse.RequestParser()
        parse.add_argument('username', type=str)
        parse.add_argument('hash', type=str)
        params = parse.parse_args()
        if sha256.verify(params['username'], params['hash']):
            current_user = UserModel.find_by_username(params['username'])
            current_user.active = 1
            current_user.save_to_db()
            return redirect(app.config['SITE_DOMAIN'] + '#/signin', code=302)


class UserLogoutAccess(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti=jti)
            revoked_token.add()
            return {'message': 'Access token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, 500


class UserLogoutRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti=jti)
            revoked_token.add()
            return {'message': 'Refresh token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, 500


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user)
        return {'access_token': access_token}


class AllUsers(Resource):
    def get(self):
        return UserModel.return_all()

    @jwt_required
    def delete(self):
        return UserModel.delete_all()


class SendLinkPasswordMail(Resource):
    def post(self):
        parse = reqparse.RequestParser()
        parse.add_argument('username', type=str)
        params = parse.parse_args()

        html = render_template('pwd_link_email.html', username=params['username'], site_domain=app.config['SITE_DOMAIN'])

        recipients = data['username']
        subject = 'Request for changing password'

        flag = send_email(app=app, recipients=recipients, subject=subject, html=html)
        if flag:

            return {
                'message': 'Mail was sent to user {} .\n Please check your mail.'.format(
                    params['username']),
            }
        else:
            return {
                'error': 'Failed mailing  for user {}.\n Please type correct email.'.format(params['username']),
            }, 400


class ChangePassword(Resource):
    def post(self):
        parse = reqparse.RequestParser()
        parse.add_argument('username', type=str)
        parse.add_argument('password', type=str)
        params = parse.parse_args()
        current_user = UserModel.find_by_username(username=params['username'])
        current_user.password = current_user.generate_hash(params['password'])
        current_user.save_to_db()
        return {
            'message': '{}!Your password is changed successfully.'.format(params['username'])
        }


class ReceiveFromLinkMail(Resource):
    def get(self):
        parse = reqparse.RequestParser()
        parse.add_argument('username', type=str)
        params = parse.parse_args()
        return redirect(app.config['SITE_DOMAIN'] + "#/resetpwd?username="+params['username'])


class MakePresignedUrl(Resource):
    @jwt_required
    def post(self):
        parse = reqparse.RequestParser()
        parse.add_argument('filename', type=str)
        parse.add_argument('file_type', type=str)
        params = parse.parse_args()
        s3Client = boto3.client('s3', aws_access_key_id=app.config['AWS_ACCESS_KEY_ID'], aws_secret_access_key=app.config['AWS_SECRET_ACCESS_KEY'])
        pre_url = s3Client.generate_presigned_url(
            'put_object',

            Params={
                'Bucket': app.config['BUCKET'],
                'Key': params['filename'],
                'ContentType': params['file_type']
            },
            ExpiresIn=600,
        )
        print(pre_url)
        return {
            'pre_url':pre_url

        }


api.add_resource(UserRegistration, '/api/registration/')
api.add_resource(UserLogin, '/api/login/')
api.add_resource(UserLogoutAccess, '/api/logout/access/')
api.add_resource(UserLogoutRefresh, '/api/logout/refresh/')
api.add_resource(TokenRefresh, '/api/token/refresh/')
api.add_resource(AllUsers, '/api/users/')

api.add_resource(MakePresignedUrl, '/api/getpresignedurl/')

api.add_resource(UserActive, '/api/active')
api.add_resource(SendLinkPasswordMail, '/api/SendMailForPassword/')
api.add_resource(ReceiveFromLinkMail, '/api/linkpwdchange/')
api.add_resource(ChangePassword, '/api/ChangePassword/')
if __name__ == '__main__':

    app.run(debug=True)