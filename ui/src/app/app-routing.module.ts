import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {NotFoundComponent} from './not-found/notfound.component';
import {FormUploadComponent} from "./source/form-upload.component";

import {UserSignUpComponent} from "./user-signup/user-signup.component";
import {UserSignInComponent} from "./user-signin/user-signin.component";
import {AuthGuard} from "./shared/guards/auth-guard.service";
import {ChangePasswordComponent} from "./user-signin/change-password.component";
import {ForgotPasswordComponent} from "./user-signin/send-passwordlink.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'sources/s3upload', component: FormUploadComponent, canActivate: [AuthGuard]},
  {path: 'signup', component: UserSignUpComponent},
  {path: 'signin', component: UserSignInComponent},
  {path: 'forgot', component: ForgotPasswordComponent},
  {path: 'resetpwd', component: ChangePasswordComponent},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {useHash: true}) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
