/*tslint:disable*/
import {Component, EventEmitter, HostListener, Inject, Injectable, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, NavigationEnd, NavigationExtras, Router} from '@angular/router';
import {UserService} from "./shared/services/user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],

})
export class AppComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,

  )
  {
  }

  ngOnInit() {}
   /**
   * Is the user logged in?
   */
  get isLoggedIn() {
    return this.userService.isLoggedIn();
  }
   /**
   * Log the user out
   */
  logout() {
    this.userService.logout().subscribe(
      data=>{
        this.router.navigate(['/signin']);
      },
      err=>{
        console.log(err)

          this.router.navigate(['/signin']);


      }
    );

  }
  get loggedInData() {
    return this.userService.getLoggedInData();
  }
}
