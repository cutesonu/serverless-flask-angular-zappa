import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HomeModule} from './home/home.module';
import {NotFoundComponent} from './not-found/notfound.component';
import {FileDropDirective, FileSelectDirective, FileUploadModule} from 'ng2-file-upload';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SourceService} from './shared/services/source.service';
import {HttpModule} from '@angular/http';
import {ToastrModule} from 'ngx-toastr';

import {FormUploadComponent} from "./source/form-upload.component";

import {UserSignUpComponent} from "./user-signup/user-signup.component";
import {UserService} from "./shared/services/user.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {UserSignInComponent} from "./user-signin/user-signin.component";
import {AuthGuard} from "./shared/guards/auth-guard.service";
import {ChangePasswordComponent} from "./user-signin/change-password.component";
import {ForgotPasswordComponent} from "./user-signin/send-passwordlink.component";


@NgModule({
  declarations: [
    AppComponent, NotFoundComponent, FormUploadComponent,  UserSignUpComponent, UserSignInComponent, ForgotPasswordComponent, ChangePasswordComponent
  ],
  imports: [
    FormsModule, BrowserModule, ReactiveFormsModule, BrowserAnimationsModule, AppRoutingModule, HomeModule, FileUploadModule, HttpModule,
    ToastrModule.forRoot()
  ],
  providers: [AuthGuard, UserService, SourceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
