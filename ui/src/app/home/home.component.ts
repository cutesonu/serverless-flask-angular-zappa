/* tslint:disable*/
import {Component, HostListener, Inject, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {DOCUMENT} from '@angular/common';
import {SourceService} from '../shared/services/source.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit{
  cloudBackgroundShow = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private sourceService:SourceService,
    private toastService:ToastrService
  ) {}

  ngOnInit() {}

  testing(){
    this.sourceService.GettingUser().subscribe(
      data => {
        this.toastService.success(data);
      },
      err => {
        this.toastService.error(err);
      }
    )
  }
}
