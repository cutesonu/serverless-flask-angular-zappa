import {Injectable} from '@angular/core';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {homeRouting} from './home.routing';
import {HomeComponent} from './home.component';
import {SourceService} from '../shared/services/source.service';


/*tslint:disable*/
@NgModule({
  imports : [
    homeRouting,
  ],
  declarations : [
    HomeComponent


  ],
  providers: [],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class HomeModule{}
