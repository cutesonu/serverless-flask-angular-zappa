import {ModuleWithProviders} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
const homeRoutes: Routes = [];
/*tslint:disable*/
export const homeRouting: ModuleWithProviders = RouterModule.forRoot(homeRoutes, {useHash: true});
