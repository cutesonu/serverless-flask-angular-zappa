import { Injectable } from '@angular/core';
import {Http, Response, RequestOptions, Headers, RequestOptionsArgs} from '@angular/http';

import {AppSettings} from '../../app.constant';
import {Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/throw';
import * as S3 from "aws-sdk/clients/s3";
import {ToastrService} from "ngx-toastr";
import {UserService} from "./user.service";
import {HttpHeaderResponse} from "@angular/common/http";

/*tslint:disable*/
@Injectable()
export class SourceService {
  private apiUrl: string = AppSettings.API_ENDPOINT ;

  constructor(
    private router: Router,
    private userService:UserService,
    private http: Http)
  {}

  GettingPresignedURL(send_data):Observable<any>{
    let access_token = localStorage.getItem('accessToken')
    let headers = new Headers({ 'Accept': 'application/json' });
    headers.append("Authorization", "Bearer " + access_token)
    let options = new RequestOptions({ headers: headers });

    return this.http.post(`${this.apiUrl}/api/getpresignedurl/`, send_data, options)
      .map(res => res.json())
      .do(res => {
        // console.log('here')
        // console.log(res)
      })
      .catch(this.handleError);
  }

  Upload_By_PresignedURL(purl, file):Observable<any>{
    const headers = new Headers({'Content-Type': file.type});
    const options = new RequestOptions({ headers: headers });
    return this.http.put(purl, file, options)
      .map((res) =>{
            console.log(res);

            return res
      } )
      .catch(this.handleError);

  }
  Process_Lambda_By_API(filename):Observable<any>{
    let lambda_api_gateway =''
    let body = {filename:filename}
    return this.http.post(lambda_api_gateway, body)
      .map((res)=> {
          console.log(res);

          return res
        }
      )
    .catch(this.handleError);
  }
  GettingUser():Observable<any> {
    let headers = new Headers({ 'Accept': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.get(`${this.apiUrl}/api/users/1`, options)
      .map(res => res.json())
      .do(res => {
        // console.log('here')
        // console.log(res)
      })
      .catch(this.handleError);
  }




  /**
   * Handle any errors from the API
   */
  private handleError(err) {
    let errMessage: string;
    if (err instanceof Response) {

      let body   = err.json() || '';
      let error  = body.error || JSON.stringify(body);
      errMessage = `${err.statusText || ''} ${error}`;
    } else {
      errMessage = err.message ? err.message : err.toString();
    }
    if(errMessage.indexOf('Token has expired') >0){
          localStorage.clear();
          this.userService.loggedInData = {}
          this.userService.loggedIn = false

      }

    return Observable.throw(errMessage);
  }
}
