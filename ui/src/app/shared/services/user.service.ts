/*tslint:disable*/
import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AppSettings} from '../../app.constant';

@Injectable()
export class UserService {
  private apiUrl: string = AppSettings.API_ENDPOINT;
  public loggedIn: boolean = false;
  public loggedInData: any = {};

  constructor(private http: Http){

    this.loggedIn = !!localStorage.getItem('accessToken');
    if(this.loggedIn)
      try {
        this.loggedInData.username = localStorage.getItem('username');
      } catch(e) {
        alert(e); // error in the above string (in this case, yes)!
      }

  }

  /**
   * Check if the user is logged in
   */
  isLoggedIn() {
    return this.loggedIn;
  }


  /**
   * Check if the user is logged in
   */
  getLoggedInData() {
    return this.loggedInData;
  }

  sendMailFor(username:string):Observable<string>{
    let body = {username:username};
    return this.http.post(`${this.apiUrl}/api/SendMailForPassword/`, body)
      .map(res => res.json())
      .do(res => {
        this.loggedIn = false;
        this.loggedInData = [];

      })
      .catch(this.handleError);
  }
  changePasswordFor(username:string, userpass:string):Observable<string>{
    let body = {
      username:username,
      password:userpass
    };
    return this.http.post(`${this.apiUrl}/api/ChangePassword/`, body)
      .map(res => res.json())
      .do(res => {
        this.loggedIn = false;
        this.loggedInData=[];
      })
      .catch(this.handleError);
  }

  /**
   * Log the user in
   */
  login(username: string, password: string): Observable<string> {
    return this.http.post(`${this.apiUrl}/api/login/`, { username, password })
      .map(res => res.json())
      .do(res => {
        if (res['accessToken'] ){
          this.loggedIn = true;
          this.loggedInData.username = username;
          localStorage.setItem('accessToken', res['accessToken']);
          localStorage.setItem('refreshToken', res['refreshToken']);
          localStorage.setItem('username', username);

        }
        else {
           this.loggedIn = false;
           this.loggedInData = [];
           localStorage.removeItem('accessToken');
           localStorage.removeItem('refreshToken');
           localStorage.removeItem('username');
        }

      })
      .catch(this.handleError);
  }


  /**
   * Log the user in
   */
  register(username: string, password: string, last_name: string, first_name: string): Observable<string> {
    let userData = {
      username : username,
      password : password,
      firstname : first_name,
      lastname : last_name
    };
    return this.http.post(`${this.apiUrl}/api/registration/`, userData)
      .map(res => res.json())
      .do(res => {

       if (res['accessToken']){
          this.loggedIn = true;
          this.loggedInData.username = username;
          localStorage.setItem('accessToken', res['accessToken']);
          localStorage.setItem('refreshToken', res['refreshToken']);
          localStorage.setItem('username', username);

        }
        else {
           this.loggedIn = false;
           this.loggedInData = [];
           localStorage.removeItem('accessToken');
           localStorage.removeItem('refreshToken');
           localStorage.removeItem('username');
        }
      })
      .catch(this.handleError);
  }

  /**
   * Log the user out
   */
  public logout():Observable<any>{
    let access_token = localStorage.getItem('accessToken')
    let headers = new Headers({ 'Accept': 'application/json' });
    headers.append("Authorization", "Bearer " + access_token)
    let options = new RequestOptions({ headers: headers });
    return this.http.post(`${this.apiUrl}/api/logout/access/`,{}, options)
    .map(res => res.json())
    .do(res => {

        localStorage.clear()
        this.loggedInData = {};
        this.loggedIn = false;
    })
    .catch(this.handleError);

  }

  /**
   * Handle any errors from the API
   */
  private handleError(err) {
    let errMessage: string;

    if (err instanceof Response) {
      let body   = err.json() || '';
      let error  = body.error || JSON.stringify(body);
      errMessage = `${err.statusText || ''} ${error}`;
    } else {
      errMessage = err.message ? err.message : err.toString();
    }
    if(errMessage.indexOf('Token') >0){
          localStorage.removeItem('accessToken');
          localStorage.removeItem('refreshToken');
          this.loggedInData = {};
          this.loggedIn = false;

      }
    return Observable.throw(errMessage);
  }



}
