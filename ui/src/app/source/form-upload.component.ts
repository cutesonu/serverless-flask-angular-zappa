import { Component, OnInit } from '@angular/core';

import {SourceService} from "../shared/services/source.service";
import {ToastrService} from "ngx-toastr";
import * as S3 from "aws-sdk/clients/s3";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
  selector: 'app-form-upload',
  templateUrl: './form-upload.component.html'

})
export class FormUploadComponent implements OnInit {
  purl:string;
  selectedFiles: FileList;
  bucket:string = 'zappa-liu';
  folder:string = 'tmp/';
  uploading:boolean = false;
  proccessing:boolean = false;

  constructor(
    private router: Router,
    private sourceService: SourceService,
    private toastService:ToastrService
  ) { }

  ngOnInit() {
    }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }


  upload() {
    this.uploading = true

    const file = this.selectedFiles.item(0);
    let send_data = { filename:file.name, file_type:file.type}

    this.sourceService.GettingPresignedURL(send_data).subscribe(
      data=>{
        this.purl = data['pre_url'];
        this.toastService.success(this.purl, 'Presigned URL:', {disableTimeOut:true, tapToDismiss:false, closeButton:true});
        this.sourceService.Upload_By_PresignedURL(this.purl, file).subscribe(
          data2=>{
            if(data2.status == 200)
              this.toastService.success('Successfully Uploaded via presigned url.','Success');
          },error2 => {
            this.toastService.error(error2);
          }
        )

      },err=>{
         console.log(err)
        if(err.indexOf('Token') >0){
          this.router.navigate(['/signin']);

        }
      }

    )
  }


  processDocument(){
    const file = this.selectedFiles.item(0);
    this.sourceService.Process_Lambda_By_API(file.name).subscribe(
      data=>{
        if(data.status == 200)
          this.toastService.success('Successfully Lambda function worked.','Success');
      }
    )

  }

}

