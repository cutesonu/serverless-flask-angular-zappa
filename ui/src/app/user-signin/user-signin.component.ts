/*tslint:disable*/
import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import {UserService} from '../shared/services/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  templateUrl: './user-signin.component.html',
  styleUrls: ['../app.component.css']
})
export class UserSignInComponent implements OnInit {
  credentials = { username: '', password: '' };
  isLoading = false;
  constructor(
    private userService: UserService,
    private router: Router,
    private toastrService: ToastrService
  ) {}
  ngOnInit() {

  }
  /**
   * Login a user
   */
  login() {

    this.isLoading = true;
    this.userService.login(this.credentials.username, this.credentials.password).subscribe(
      data => {
        this.isLoading = false;
        if(data['accessToken']){
          this.toastrService.success(data['message'], 'Success');
          this.router.navigate(['/sources/s3upload']);
        }else {
          this.toastrService.error(data['message'], 'Failed');
        }

        },
        err => {
        this.isLoading = false;
        // if (err.indexOf("Unauthorized") != -1)
        this.toastrService.error(err, 'Error');
      }
      );
  }

}
